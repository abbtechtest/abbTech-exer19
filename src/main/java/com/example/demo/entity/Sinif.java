package com.example.demo.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Sinif")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Sinif {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sinif_id")
    Integer sinifId;

    @Column(name = "name", length = 10)
    String name;

    @ManyToMany
    @JoinTable(
            name = "studentsinif",
            joinColumns = @JoinColumn(name = "sinif_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id")
    )
    private Set<Student> students = new HashSet<>();

}

