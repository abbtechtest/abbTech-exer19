package com.example.demo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
@Embeddable
public class StudentSinif {
    @Column(name = "student_id")
    Integer studentId;

    @Column(name = "sinif_id")
    Integer sinifId;
}

