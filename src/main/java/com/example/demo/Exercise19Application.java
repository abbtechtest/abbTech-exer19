package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exercise19Application {

    public static void main(String[] args) {
        SpringApplication.run(Exercise19Application.class, args);
    }

}
