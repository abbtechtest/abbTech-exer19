package com.example.demo.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
  @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Entity
    @Table(name = "Professor")
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public class Professor {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "professor_id")
        private Long professorId;

        @Column(name = "first_name", length = 50)
        private String firstName;

        @Column(name = "last_name", length = 50)
        private String lastName;

        @ManyToOne
        @JoinColumn(name = "department_id")
        private Department department;

        @ManyToOne
        @JoinColumn(name = "book_id")
        private Book book;
    }



